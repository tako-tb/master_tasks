#Lecture 2
# print("---Lecture 2---")

# x = input("x=")
# # print(x)
#
# x = int(input("x="))
# print(x*3)


## if:
# if x>10:
#     print(x-5)
#     print(x+5)
#
# # below request is out of "IF"
# print(x*2)


# for:
# for i in range (0, 5): is the same syntax
# for i in range (5):
#     print("Hello!!!")
#     print(i)
#     print("i*3="+ str(i*3))


# x = int(input("x="))
# for i in range (0, x):
#     print("Hello!!!")
#     print(i)
#     print("i*3="+ str(i*3))




# # while & break operator:
#
# i = 1
# while i < 6:
#     print(i)
#     if i ==3:
#         break #3-ის ტოლი რომ გახდება გაწყდება ციკლი
#     i+=1

# # for & list:
# names = ["Ana", "Bacho", "Vaxo", "Vika"]
# print("Hello "+names[0]+"!!!")
#
# for e in names:
#     print("Hello " + e + "!!!")
#
# for i in range(0, len(names)):
#     print("Hello " + names[i]  + "!!!")


# # Lists (კოლექციები):
# e.g. 34 is age.
# x = 34
# print(x)

# # if we want data like: name, age, etc
# # then we need separate integers or string:

# name = "Avto"
# lastname = "Bichinashvili"
# age = 34
# gender = "Male"
# # that is not comfortable

# შეგვიძლია ასე:
#
# person = ["Avto", "Bichinashvili", 34, "Male", "Avto", 3, 38, "Gvalia"]
# print(person)
# print(person[2])
# #
# #
# for element in person:
#     print(element)
#
# person[2] = 47
# print(person)

#
# # add element in the list - append ამატებს სულ ბოლოში
# person.append(87)
# print(person)
#
# # insert-ს სჭირდება იმ ელემენტის ინდექსი, რომლის მერეც ამატებს:
# person.insert(2, "Bichia")
# print(person)

#
# # Tuples (collection): is the same as lists but has specifics:
#
# person = ("Avto", "Bichinashvili", 34, "Male")
# print(person)
# person = ("Avto", "Bichinashvili", 34, "Male",34)
# print(person)
# print(person[1])
# # person[2] = 47
# print(person)

# # the only difference is that we cannot change any element!!!
# # შეგვიძლია tuple გარდავქმნათ ლისტად
# # > შევცვალოთ მონაცემი და ისევ გარდავქმნათ ტუპლე-ად
# personList = list(person)
# print(personList)


# Set: არ ბეჭდავს იმ თანმიმდევრობით, როგორც შეყვანილია:
# ვერ მივწვდებით თითოეულ ელემენტებს და ვერ შევცვლით მონაცემებს,
# ასევე,ერთი და იგივე მონაცემებს ვერ ჩავწერთ ორჯერ ან მეტჯერ

# person = {"Avto", "Bichinashvili", 34, 9, 0, "Male"}
# print(person)
# #print(person[1]) error
#
# # უნიკალური მონცემების ამოღება შეგვიძლია ლისტიდან:
# l = [3,4,5,3,4,1,4,4,8]
# print(l)
# s = set(l)
# print(s)  # შედეგი გვექნება მხოლოდ უნიკალური მონცემები

# for / edit/ insert შეიძლება


# Functions: კოდის ნაწილი გვქონდეს ცალკე შენახულად
# და როცა დაგვჭირდება გამოვიძახოთ.
# მაგალითად: ინტ ფუნქციაა, ინპუტიც, პრინტიც, რეინჯიც, სტრინგიც
# თუ .ხხხ > წერტილის მერეა, მაშინ უკვე მეთოდია
# ფუნქცია იწერება def-ით


# def print_name()

# def f1():
#     print("Python !!!") # შედეგი არ გვაქვს. უნდა გამოვიძახოთ!
#     x = input("X = ")
#     print(x)

# # f1()
# # x, y, z არიან არგუმენტები
# def f2(x, y, z):
#     print(x+y+z)
#
# f2(3,4,5)
# f2(5,63,98)
# f2(int(input()), int(input()),int(input()))
#
# f2() # error იქნება რადგან ცარიელია,
# # ამიტომ def f2(x=0, y=0, z=0) შეგვიძლია თავიდანვე მივანიჭოთ

# def f3 ():
#     return 89 # return
#     x=9
#     print(x+9)
#
# f3()