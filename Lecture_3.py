# # # Dictionaries
#
# l = ["Ford", "Mustang", 1964]
# # print((l[1]))
# # dictionaries:
#
# dic = {
#     "brand": "Ford",
#     "model": "Mustang",
#     "year": 1964,
#     "model": "Fiesta",
#     "model": "Escape",
#     "engine": [3.0, 2.5, 2.0, 6.0]
#
# }
#
# dic ["engine"] = 3.0
#
#
# # print(dic["model"])
#
# #  key value -პრინციპი, არ უნდა გვქონდეს რამოდენიმე ქი ველიუ, თუ გვაქვს გადააწერს
#
# for e in dic:
#     print((e+" - " + str(dic[e])))



#  ფაილებთან მუშაობა:
#  შექმნა:
# f = open("demofile2.txt", "a")
# f.write("Now the file has more content")
# f.write("\nPython!")
# f.close()

#
# f = open("demofile2.txt", "r")
# print(f.read())
# print(f.read(30))

# print(f.readline())
# print(f.readline())
#
# for l in f:
#     print("----"+l)
#
#

#f.close()
#print(f.readline()) -- შეცდომა, ფაილი დახურულია




import os
# os.remove("demofile2.txt")

print (os.path.exists("d://test.txt"))

os.rmdir("test")