# #1. შეიტანეთ ათწილადი რიცხვი, დაამრგვალეთ ათწილად ნაწილში მეათედის სიზუსტით (1 ციფრი ათწილად ნაწილში) და
# #დაბეჭდეთ შედეგი.
# #გამოიყენეთ round, ceil, floor, trunc ფუნქციები სათითაოდ და შეამოწმეთ შედეგი თითოეულის გამოყენებით.
# import math
#
# x = float(input("Enter a number: "))
# r = round(x, 3)
# c = math.ceil(x)
# f = math.floor(x)
# t = math.trunc(x)
#
# print(r)
# print(c)
# print(f)
# print(t)
from random import random

# #2. შეიტანეთ სამი რიცხვი, იპოვეთ მათ შორის მაქსიმუმი და დაბეჭდეთ შედეგი. გამოიყენეთ max ფუნქცია.

# x = int(input("Enter a number: "))
# y = int(input("Enter a number: "))
# z = int(input("Enter a number: "))
#
# m = max(x, y, z)
# print(m)
#


# #4. დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი დიაპაზონიდან 0-დან 1-ის ჩათვლით.
# #დაამრგვალეთ რიცხვი (3 ციფრი ათწილად ნაწილში) და დაბეჭდეთ.

# import random
#
# print(round(random.uniform(0, 1), 3))


# #5. დააგენერირეთ ნებისმიერი შემთხვევითი ათწილადი რიცხვი 100-დან 120-მდე.
# #დაამრგვალეთ რიცხვი (1 ციფრი ათწილად ნაწილში) და ისე გამოიტანეთ.
#
#
# import random
#
# print(round(random.uniform(100, 120), 1))


# #6. დააგენერირეთ 10 შემთხვევითი მთელი რიცხვი და დაბეჭდეთ ეკრანზე. მითითება:
# #გამოიყენეთ ციკლის ოპერატორი.

# import random
#
# for i in range (10):
#     print(random.randint(3, 9))


# #7. შექმენით ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის (დააბრუნებს) მათ საშუალო არითმეტიკულს.
# გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის და დაბეჭდეთ შედეგი.

# def average(a, b):
#     return (a + b) / 2
#
#
# for i in range(3):
#     x = int(input("Enter a number: "))
#     y = int(input("Enter a number: "))
#     print(average(x,y))


# #8. დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა ორი რიცხვი და დაითვლის მათ საშუალო არითმეტიკულს და დაბეჭდავს შედეგს
# (გაითვალისწინეთ რომ დაბეჭდვა უნდა მოხდეს ფუნქციის შიგნით - ფუნქცია არ აბრუნებს მნიშვნელობას).
# გამოიძახეთ ფუნქცია 3-ჯერ სხვადასხვა რიცხვებისთვის.


# def average(a, b):
#     print((a + b) / 2)
#
#
# for i in range(3):
#     x = int(input("Enter a number: "))
#     y = int(input("Enter a number: "))
#     average(x,y)


# #9. შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) არგუმენტად გადაცემული რიცხვის კუბს.
# გამოიძახეთ ფუნქცია რამდენიმეჯერ და დაბეჭდეთ მიღებული შედეგი.

# def cube(a):
#     return a * a * a
#
# x = int(input("Enter a number: "))
# print(cube(x))


# #10. შექმენით ფუნქცია, რომელიც დაითვლის (დააბრუნებს) ორ რიცხვს შორის მინიმალურ მნიშვნელობას.
# გამოიძახეთ ფუნქცია და დაბეჭდეთ შედეგი (პარამეტრად გადაეცით ნებისმიერი ორი რიცხვი).

# def min_num(x, y):
#     return min(x, y)
#
#
# x = int(input("Enter a number: "))
# y = int(input("Enter a number: "))
# print(min_num(x, y))


# #11. დაწერეთ ფუნქცია, რომელიც შეამოწმებს პარამეტრად გადაცემული რიცხვი არის თუ არა კენტი.
# #თუ კენტია, დააბრუნოს მნიშვნელობა True, თუ არადა - False. შეამოწმეთ რამდენიმე რიცხვისთვის და დაბეჭდეთ შედეგი.

#
# def odd_num(a):
#     if a % 2 != 0:
#         return "True"
#     else:
#         return "False"
#
# k = int(input("Enter a number: "))
# print(odd_num(k))


# #12. დაწერეთ ფუნქცია, რომელიც დაითვლის (დააბრუნებს) პარამეტრად გადაცემული რიცხვის ფაქტორიალს
# #და დაბეჭდეთ შედეგი სხვადასხვა რიცხვებისთვის.

# def factorial(n):
#         if n < 0:
#             return 0
#         elif n == 0 or n == 1:
#             return 1
#         else:
#             fact = 1
#             while (n > 1):
#                 fact *= n
#                 n -= 1
#             return fact
#
#
# for i in range(3):
#     k = int(input("Enter a number: "))
#     print(factorial(k))




# #13. დაწერეთ უპარამეტრო ფუნქცია რომელიც ეკრანზე ბეჭდავს შემდეგ ტექსტს: “Hello World”.
# (გაითვალისწინეთ რომ ფუნქცია არ აბრუნებს მნიშვნელობას).

# def my_text():
#     print('Hello World')
#
#
# my_text()


# #14. დაწერეთ ანონიმური ფუნქცია რომელიც დაითვლის რიცხვის კუბს.

# f = lambda a: a*a*a
# k = int(input("Enter a number: "))
# result = f(k)
# print(result)


# #15. შექმენით სია numbs ნებისმიერ 5 რიცხვითი მნიშვნელობით. იპოვეთ ამ რიცხვების
# ჯამი, მინიმალური, მაქსიმალური და საშუალო არითმეტიკული. ასევე შეასრულეთ
# შემდეგი ოპერაციები:
# • სიას დაამატეთ ბოლო ელემენტად რიცხვი 102
# • სიის მესამე ელემენტად ჩასვით რიცხვი 205
# • წაშალეთ სიის მე-4 ელემენტი
# • დაალაგეთ სია ზრდადობის მიხედვით და დაბეჭდეთ.


# numbs = [5,8,21,66,13]
# print(numbs)
#
# print(sum(numbs))
# print(max(numbs))
# print(min(numbs))
# print(sum(numbs)/len(numbs))
#
# numbs.append(102)
# print(numbs)
#
# numbs.insert(3,205)
# print(numbs)
#
# del numbs[4]
# print(numbs)
#
# numbs.sort()
# print(numbs)
#
# # # დალაგებული კლებადობის მიხედვით:
# numbs.sort(reverse=True)
# print(numbs)


# #16. დაწერეთ პროგრამა, რომლის მეშვეობით შეიყვანთ (input-ით) 10 მონაცემს.
# წარმოადგინეთ და დაბეჭდეთ ისინი სიის ელემენტების სახის.

# my_list = []
#
# for i in range(0, 10):
#     element = int(input("Enter a number: "))
#     my_list.append(element )
#
# print(my_list)




# 17. შექმენით სია fruits, რომელის ელემენტებია: Watermelon, Banana, Apple.
# დაალაგეთ სიის ელემენტები ალფაბეტის უკუ-მიმართულებით და დაბეჭდეთ ისინი.

# fruits = ["Apple","Watermelon","Banana"]
# fruits.sort(reverse=True)
# print(fruits)


# 18. დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა სია, დაითვლის სიის ელემენტების ნამრავლს და დააბრუნებს შედეგს.
# გამოიძახეთ ფუნქცია ნებისმიერი სიისთვის.

# def multiplyList(my_List):
#     result = 1
#     for i in my_List:
#         result = result * i
#     return result
#
#
# list1 = [1, 2, 3]
# list2 = [8, 11, 44]
# list3 = [12, 2, 18]
#
# print(multiplyList(list1))
# print(multiplyList(list2))
# print(multiplyList(list3))


# 19. დაწერეთ პროგრამა, რომელიც რიცხვითი მნიშვნელობების სიაში ამოშლის კენტ რიცხვებს.
# დაბეჭდეთ მიღებული სია.

# my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# for i in my_list:
#     if i % 2 != 0:
#         my_list.remove(i)
# print(my_list)


# 20. შექმენით 5 ელემენტიანი სია (რიცხვითი მნიშვნელობებით).
# თითოეული ელემენტი გაზარდეთ 10-ით და დაბეჭდეთ სია.

# my_list = [2,5,6,8,12]
#
# new_list = [x+10 for x in my_list]
# print(new_list)


# 21. შექმენით 10 ელემენტიანი სია, რომლის ელემენტებია ნებისმიერი შემთხვევითი მთელი რიცხვები 25-დან 110-მდე.
# დაბეჭდეთ სია და იპოვეთ მინიმალური ელემენტი.

# import random
#
# r_list=[]
# n=10
# for i in range(n):
#     r_list.append(random.randint(25,110))
# print(r_list,"; Minimum number is: ",min(r_list))

# #22. დაწერეთ ფუნქცია, რომელსაც არგუმენტად გადაეცემა 2 სია.
# # ფუნქცია აბრუნებს მნიშვნელობა True-ს თუ სიებს აქვთ ერთი მაინც საერთო ელემენტი.
# # წინააღმდეგ შემთხვევაში აბრუნებს False მნიშვნელობას.

# def same_num(list1, list2):
#     for x in list2:
#         if x in list1:
#             return True
#     return False
#
# print(same_num([11, 2, 3, 33, 4, 5], [78, 6, 7, 8, 9, 33]))



# 23. დაწერეთ ფუნქცია, რომელიც სიაში არსებულ კენტ რიცხვებს წაშლის.
# ფუნქცია აბრუნებს განახლებულ სიას.

# def remove_odd(my_list):
#     for i in my_list:
#         if i % 2 != 0:
#             my_list.remove(i)
#     print(my_list)
#     return my_list
#
#
# list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# print(remove_odd(list1))


#24. შექმენით სია რიცხვითი ელემენტებით. shuffle ფუნქციის გამოყენებით (random მოდულიდან) მოახდინეთ სიის ელემენტების
# შემთხვევითად არევა და დაბეჭდეთ მიღებული სია.
# (მითითება: ფუნქცია იწერება შემდეგნაირად: random.shuffle(x) სადაც x სიის დასახელებაა)

#
# import random
#
# def shuffleList(my_list):
#     random.shuffle(my_list)
#     print(my_list)
#
# shuffleList([5,6,8,9,12,56])



# #25. შექმენით სია რიცხვითი მნიშვნელობებით.
# რანდომულად ამოარჩიეთ სიის რომელიმე ელემენტი და დაბეჭდეთ.
# (მითითება: წინა სავარჯიშოს მსგავსად გამოიყენეთ random მოდულის choice ფუნქცია).

# import random
# my_list = [2,45,12,99,15,46]
#
# print(random.choice(my_list))


# #26. დაწერეთ პროგრამა, რომელშიც შეიტანთ (input-ით) ნებისმიერ დიდ რიცხვს
# (მაგ. 342387410984). იპოვეთ რიცხვის ციფრთა ჯამი.
# (მითითება: თავდაპირველად გარდაქმენით რიცხვი სიად).
#
#
# num = int(input("Enter a big number: "))
# my_list = [int(x) for x in str(num)]
#
# print(sum(my_list))


# #27. იპოვეთ სიაში [1, 5, 23, 5, 12, 2, 5, 1, 18, 5] ყველაზე ხშირად განმეორებადი რიცხვი.
# #დაბეჭდეთ შედეგი. ასევე მიუთითეთ რამდენჯერ შეგხვდათ სიაში ყველაზე ხშირად განმეორებადი რიცხვი.
#
#
# ## Mode of list:
# import statistics
#
# my_list = [1, 5, 23, 5, 12, 2, 5, 1, 18, 5]
#
#
# # freq_element = max(set(my_list), key = my_list.count)
# # print(freq_element)
# # x = my_list.count(freq_element)
# # print(x)
#
#
# # Median of list:
# my_list.sort()
# print(my_list)
#
# mid = len(my_list) // 2
# median = (my_list[mid] + my_list[~mid]) / 2
#
# print("Median of list is : " + str(median))
#
#
#
# # Mean of list:
# m = sum(my_list)/len(my_list)
# print(m)
#
#
#
# # using statistics:
#
# print(statistics.median(my_list))
# print(statistics.mode(my_list))
# print(statistics.mean(my_list))




# # 28. შექმენით სია extensions = ['txt';, 'jpg';'gif';'html';]..
# # პროგრამის გაშვების შემდეგ მომხამრებელმა შეიყვანოს (input) ნებისმიერი ფაილის დასახელება.
# # თუ ფაილის გაფართოება ემთხევა სიის რომელიმე ელემენტს, დაბეჭდოს ეკრანზე “Yes”, წინააღმდეგ შემთხვავაში დაბეჭდოს “No”.
#
#
# extensions = ['(.txt)','(.jpg)','(.gif)','(.html)']
# fileName = str(input("Enter a file name: "))
#
# newName = fileName.split()
#
# def test_includes_any(nums, lsts):
#     for x in lsts:
#         if x in nums:
#             return True
#     return False
#
# print(test_includes_any(extensions, newName))



# 29. სტრიქონი python php pascal javascript java c++; წარმოადგინეთ სიის სახით
# (სტრიქონის თითოეული სიტყვა სიის თითოეული ელემენტად).
# იპოვეთ სიის ყველაზე გრძელი ელემენტი (ანუ ყველაზე გრძელი სიტყვა).


# coding = "python php pascal javascript java c++"
# newName = coding.split()
# print(newName)
#
# longest_name = max(newName, key=len)
# print("Longest name has: ", longest_name)



# 30. შეიტანეთ სიის 10 ელემენტი. იპოვეთ ამ რიცხვების საშუალო არითმეტიკული,მედიანა და მოდა.
# გაითვალისწინეთ, მედიანა წარმოადგენს შუა ელემენტს,როდესაც რიცხვები დალაგებულია ზრდადობით (ან კლებადობით);
# თუ შუაში ორი ელემენტია, მაშინ მედიანა არის ამ შუა ელემენტების საშუალო არითმეტიკული.
# მოდა, არის მიმდევრობაში რიცხვი, რომელიც ყველაზე ხშირად გვხვდება.
# შეიძლება მიმდევრობას არ ქონდეს მოდა (თუ ყველა ელემენტს ერთნაირი სიხშირე აქვს), ან ქონდეს ერთი ან რამდენიმე მოდა.


# my_list = []
# for i in range(0, 10):
#     element = int(input("Enter a number: "))
#     my_list.append(element )
#
# print(my_list)
#
#
# freq_element = max(set(my_list), key = my_list.count)
# print("Mode of list is : ", freq_element)
#
#
# # Median of list:
# my_list.sort()
# mid = len(my_list) // 2
# median = (my_list[mid] + my_list[~mid]) / 2
#
# print("Median of list is : ", str(median))
#
#
# # Mean of list:
# m = sum(my_list)/len(my_list)
# print("Mean of list is: ", m)
