# #1) გამოთვალეთ შემდეგი გამოსახულება 4-2^3 +5*2-3/2 და დაბეჭდეთ შედეგი ეკრანზე.

# a=4
# b=2
# c=5
# d=3
# print(a-pow(b, 3)+c*2-d/2)


# #2) დაწერეთ პროგრამა, რომელიც შეყვანილ ათობით რიცხვს გადაიყვანს ორობითში.

# x = int(input("Enter a number: "))
# bin_x = bin(x)
# print(bin_x)


# #3) დაწერეთ პროგრამა, რომელიც user-ს შეეკითხება თვეში სამუშაო საათების რაოდენობას და საათობრივ სახელფასო
# #განაკვეთს. შედეგად დაბეჭდავს ყოველთვიური ხელფასის ოდენობას.
#
# x = input("Enter monthly work hours: ")
# y = input("Enter hourly rate: ")
# print(int(x)*int(y))
#


# #4) დაწერეთ პროგრამა, სადაც მომხმარებელი შეიყვანს 3 რიცხვს და პროგრამა დაბეჭდავს რიცხვების საშუალო არითმეტიკულს.
# x = int(input("Enter 1st number: "))
# y = int(input("Enter 2nd number: "))
# z = int(input("Enter 3rd number: "))
#
# numbers = [x, y, z]
# average = sum(numbers)/len(numbers)
# print(average)
# # print((x+y+z)/3)


# #5) დაწერეთ პროგრამა რომელიც შეეკითხება user-ს სახელს და ასაკს. გამოთვალეთ რამდენ წელში გახდება 100 წლის
# #და დაბეჭდეთ შესაბამისი ინფორმაცია.
#
# a = int(input("Enter your age: "))
# print(100-a)


# #6) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ნებისმიერ რიცხვს. თუ რიცხვი დადებითია, დაბეჭდოს ეკრანზე
# #“Number is positive”.

# x = int(input("Enter a number: "))
# if x > 0:
#     print("Number is positive")


# #7)დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ნებისმიერ რიცხვს. პროგრამამ შეამოწმოს, თუ შეყვანილი
# # რიცხვი 10-ის ჯერადია, დაბეჭდოს “რიცხვი ბოლოვდება 0-ით”, თუ არადა დაბეჭდოს “რიცხვი არ ბოლოვდება 0-ით”.
# # (გაითვალისწინეთ: 10-ის ჯერადი ნიშნავს რომ 10-ზე გაყოფისას ნაშთი არის 0).
#
#
# x = int(input("Enter a number: "))
# if x % 10 == 0:
#     print("რიცხვი ბოლოვდება 0-ით")
# else:
#     print("რიცხვი არ ბოლოვდება 0-ით")


# #8)დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ ორ ნებისმიერ რიცხვს. პროგრამამ შეამოწმოს, თუ ორივე
# #შეყვანილი რიცხვი 10-ზე მეტია, დაითვალეთ მათი საშუალო არითმეტიკული, თუ არადა დაითვალეთ მათი
# #ნამრავლი. დაბეჭდეთ მიღებული შედეგი.

# x = int(input("Enter 1st number: "))
# y = int(input("Enter 2nd number: "))
#
#
# if x>10 and y>10:
#      print((x+y)/2)
#
# else: print(x*y)


# #9) დაწერეთ პროგრამა, რომლის გაშვებისას შეიყვანთ ნებისმიერ რიცხვს.
# #იპოვეთ შეყვანილი რიცხვის ბოლო ციფრი და დაბეჭდეთ ეკრანზე.
#
# a = int(input("Enter a number: "))
# print(a % 10)



# #10) დაწერეთ პროგრამა, რომლის მეშვეობითაც შეიტანთ წელს და დაადგენთ არის თუ არა შეყვანილი რიცხვი ნაკიანი
##წელიწადი. მაგ: 2012, 2016 წლები ნაკიანია.
## გაითვალისწინეთ, ნაკიანია წელიწადი, რომელიც უნაშთოდ იყოფა ოთხზე, გარდა იმ წლებისა რომლებიც იყოფა 100-ზე
## მაგრამ არ იყოფა 400-ზე.
## მაგ. 2100, 2200, 2300 წლები არ არის ნაკიანი. 2000 წელი ნაკიანია.
## ლექციაზე ერთად ამოხსნილი მითითებით
#
# x = int(input("Enter a year: "))
#
# if (x % 4 == 0 and x % 100 != 0) or (x % 400 == 0):
#         print("აღნიშნული წელიწადი ნაკიანია")
# else:
#         print("აღნიშნული წელიწადი არ არის ნაკიანი")



# #11) დაბეჭდეთ 5-ის ჯერადი რიცხვები 20-დან 125-ის ჩათვლით.
#
# i = 5
# while i <= 125:
#     print(i)
#     i+=5


# #12) დაბეჭდეთ 8-ის ჯერადი რიცხვები 200-დან 25-ის ჩათვლით კლებადობით.
#
# i = 200
# while i >= 25:
#     print(i)
#     i-=8


# #13) შეიყვანთ 2 რიცხვი. ციკლის გამოყენებით დაბეჭდეთ შეყვანილი რიცხვების საერთო გამყოფები.
# მაგ. 15-ის და 18-ის საერთო გამყოფებია: 1, 3

# # ევკლიდეს ალგორითმი ვნახო
# #
# a = int(input("Enter a number: "))
# b = int(input("Enter a number: "))
# i = 0
#
# for i in range(1, min (a,b)+1):
#     if a % i == b % i == 0:
#         print(i)



# #14) შეიყვანეთ 10 რიცხვი კლავიატურიდან ციკლის გამოყენებით. დაითვალეთ შეყვანილი რიცხვების საშუალო არითმეტიკული.
#
# i = 1
# sum = 0
#
# while i <= 10:
#     x = int(input("Enter a number: "))
#     i+=1
#     sum+=x
#
# print(sum/(i-1))
#



# #15) დაითვალეთ 1-დან 100-ის ჩათვლით ლუწი რიცხვების ჯამი და დაბეჭდეთ შედეგი.
#
# i = 1
# k = 0
#
# while i <= 100:
#     if i % 2 == 0:
#      k+=i
#     i+=1
#
# print(k)


# #16) დაბეჭდეთ 1500-დან 2100-ის ჩათვლით რიცხვები რომლებიც არიან 7-ის და 5-ის ჯერადი ერთდროულად.

# i = 1500
# while i <= 2100:
#     if i % 35 == 0:
#         print(i)
#     i+=1


# #17) დაითვალეთ 1500-დან 2100-ის ჩათვლით რიცხვების ჯამი რომლებიც არიან 7-ის და 5-ის ჯერადი ერთდროულად.
# #დაბეჭდეთ მიღებული შედეგი.

# i = 1500
# s = 0
# while i <= 2100:
#     if i % 35 == 0:
#         s +=i
#     i+=1
# print(s)



# #18) დაითვალეთ 1500-დან 2100-ის ჩათვლით რიცხვების ჯამი რომლებიც არიან 7-ის და 5-ის ჯერადი ერთდროულად.
# როგორც კი რიცხვების ჯამი გადააჭარბებს 20 000-ს, შეწყვიტეთ ციკლი. დაბეჭდეთ მიღებული ჯამი ეკრანზე.

# i = 1500
# s = 0
# while i <= 2100:
#     if i % 35 == 0:
#         s +=i
#         if s >= 20000:
#             break
#     i+=1
# print(s)



# #19) დაითვალეთ 1500-დან 2100-ის ჩათვლით 5-ის ჯერადი რიცხვების რაოდენობა. დაბეჭდეთ შედეგი.
#
# i = 1500
# c = 0
# while i <= 2100:
#     if i % 5 == 0:
#         c+=1
#     i+=1
#
# print(c)


# #20) დაბეჭდეთ ეკრანზე 15-დან 150-მდე 5-ის ჯერადი რიცხვები გარდა 50-ის, 75, 80-ისა.
# გამოიყენეთ continue ოპერატორი.

# i = 15
# while i <= 150:
#     i += 1
#     if i % 5 == 0:
#
#         if i == 50:
#             continue
#
#         if i == 75:
#             continue
#
#         if i == 80:
#             continue
#
#         print(i)



# #21) შეიყვანეთ 2 დადებითი მთელი რიცხვი. იპოვეთ ამ ორი რიცხვის უდიდესი საერთო გამყოფი.
#
# a = int(input("Enter a positive number: "))
# b = int(input("Enter a positive number: "))
# i = 0
# x = 1
# for i in range(1, min (a,b)+1):
#     if a % i == b % i == 0:
#         x = i
# print(x)


# 22) შეიყვანეთ 2 დადებითი მთელი რიცხვი. იპოვეთ ამ ორი რიცხვის უმცირესი საერთო ჯერადი.
#
# a = int(input("Enter a positive number: "))
# b = int(input("Enter a positive number: "))
#
# for i in range(max(a, b), 1 + (a * b)):
#     if i % a == i % b == 0:
#         c = i
#         break
#
# print(c)

# #?????????????23) შეიყვანეთ 10 რიცხვი ციკლის გამოყენებით. იპოვეთ რიცხვებს შორის უდიდესი კენტი რიცხვი და დაბეჭდეთ.
# თუ კენტი რიცხვი არ შეგხვდათ, გამოიტანეთ შესაბამისი შეტყობინება.



# n = 0
# num = 0
# maxnum = 0
#
# while n < 3:
#     num = int(input("Enter a number: "))
#     if num % 2 != 0:
#         if num > maxnum:
#             maxnum = num
#
#     n += 1
# print(maxnum)


# #24) შეიყვანეთ რიცხვი კლავიატურიდან. პროგრამამ უნდა დაბეჭდოს შეყვანილი რიცხვის ყველა გამყოფი.
# (მაგ. 18-ის გამყოფებია: 1, 2, 3, 6, 9, 18)
#
# a = int(input("Enter a number: "))
#
# for i in range(1, a+1):
#     if a % i == 0:
#         print(i)



# #25) შეიტანეთ რიცხვი. დაადგინეთ შეტანილი რიცხვი არის თუ არა მარტივი რიცხვი და გამოიტანეთ შესაბამისი
# #შეტყობინება (გაითვლისწინეთ: მარტივია რიცხვი, რომელსაც აქვს მხოლოდ ორი გამყოფი: ერთი და თავისი თავი).
#
# a = int(input("Enter a number: "))
#
# if a > 1:
#     for i in range(2, a):
#         if (a % i) == 0:
#             print("is not a prime number")
#             break
#     else:
#         print("is a prime number")
#
# else:
#     print("is not a prime number")


# # 26) დაბეჭდეთ 2-დან 1000-მდე ყველა მარტივი რიცხვი.
#
# for num in range(2, 1000+1):
#     if num > 1:
#         for i in range(2, num):
#             if num % i == 0:
#                 break
#         else:
#             print(num)


# #27) დაბეჭდეთ 0-დან 100-მდე არსებული ფიბონაჩის რიცხვები (ფიბონაჩის რიცხვებია 1, 1, 2, 3, 5, 8, 13, ...).
#
# x = 0
# y = 1
# z = 1
#
# while y < 100:
#     print(y)
#     x = y
#     y = z
#     z = x+y


# #28) შეიყვანეთ ნებისმიერი რიცხი. იპოვეთ ამ რიცხვის ციფრთა რაოდენობა და დაბეჭდეთ.
#
# a = int(input("Enter a number: "))
# print(len(str(a)))



# #29) შეიყვანეთ ნებისმიერი რიცხი. იპოვეთ ამ რიცხვის ციფრთა ჯამი და დაბეჭდეთ.
#
# a = int(input("Enter a number: "))
# s = str(a)
# sum = 0
#
# for i in s:
#     sum +=int(i)
#
# print(sum)


# # 30) შეიყვანეთ ნებისმიერი რიცხი. დაბეჭდეთ შეტანილი რიცხვი შებრუნებული სახით.
# # (მითითება: მაგ. თუ შეიტანთ 1254-ს, დაბეჭდოს 4521)
#
#
# num = int(input("Enter a number with at least 2 digits: "))
# opposite = 0
#
# while num > 0:
#     Reminder = num % 10
#     opposite = (opposite * 10) + Reminder
#     num = num //10
#
# print(opposite)

# # 31) შეიყვანეთ ნებისმიერი რიცხი. დაადგინეთ არის თუ არა შეტანილი რიცხვი პალინდრომი.
# # მითითება: პალინდრომია რიცხვი, რომელიც მარჯვნიდან და მარცხნიდან ერთნაირად იკითხება). მაგ. 12521
#
#
# num = int(input("Enter a number with at least 3 digits: "))
# newnum = num
# opposite = 0
#
# while newnum > 0:
#     remainder = newnum % 10
#     opposite = (opposite * 10) + remainder
#     newnum = newnum // 10
#
# if num == opposite:
#   print('Palindrome')
#
# else:
#   print("Not Palindrome")


# #32) შეიყვანეთ რიცხვი. დათვალეთ ამ რიცხვის ფაქტორიალი და დაბეჭდეთ. მაგ. 5-ის ფაქტორიალი იგივია რაც
#
# num = int(input("Enter a number: "))
#
# f = 1
# while num > 0:
#     f *= num
#     num -=1
# print(f)
#


#33) დაწერეთ პროგრამა რომელიც ეკრანზე გამოიტანს შემდეგ გამოსახულებას. მითითება:
#გამოიყენეთ 2 ჩადგმული for ციკლი.


